CXX = clang++
FLAGS = 
WARNINGS = -Wall -Wextra -Wno-unused-parameter

ALL:
	$(CXX) $(WARNINGS) -g -D DEBUG -O0 -c src/main.cpp -o build/main.o
	$(CXX) $(FLAGS) -O0 build/*.o -o bwt

release:
	$(CXX) $(WARNINGS) -O3 -c src/main.cpp -o build/main.o
	$(CXX) $(FLAGS) -O3 build/*.o -o bwt
	@strip bwt
