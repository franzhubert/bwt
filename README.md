# bwt - Band Width Test

bwt is a reliable memory bandwidth test that can run on multiple cores to get the most bandwidth and tests throughput in a couple different scenarios.


## Compile

Debian Linux:

* $ sudo apt install make build-essentials
* go to any folder where you wanna have the repository stored locally
* git clone https://gitlab.com/franzhubert/bwt.git
* $ cd bwt
* $ sudo apt install make build-essentials
* $ make prepare
* $ make release
* execute via $ ./bwt
