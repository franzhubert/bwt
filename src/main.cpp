
#include "allinc.hpp"
#include "defines.hpp"

using namespace std;
using namespace std::chrono;

// Write tests
auto testWriteMemset(uint64_t* a, uint64_t size)
{
	memset(a, 1, size); 
}

auto testWriteDumb(uint64_t* a, uint64_t size)
{
	for(uint64_t i=0; i<size; i++)
	{
		a[i] = 1;
	}
}

// Copy tests
auto testCopyMemcpy(uint64_t* a, uint64_t* b, uint64_t size)
{
	memcpy(a, b, size);
}
/*
auto testCopyMempcpy(uint64_t* a, uint64_t* b, uint64_t size)
{
	mempcpy(a, b, size, 4096);
}
*/
auto testCopyDumb(uint64_t* a, uint64_t* b, uint64_t size)
{
	for(uint64_t i=0; i<size; i++)
		b[i] = a[i];
}

auto testCopyByPointer(uint64_t* a, uint64_t* b, uint64_t* c, uint64_t size)
{
	for(uint64_t i=0; i<size; i++)
		a[i] = b[c[i]];
}

// Read tests
auto testReadDumb(uint64_t* a, uint64_t size)
{
	uint64_t num;
	for(uint64_t i=0; i<size; i++)
		num = a[i];
}

auto testReadByPointer(uint64_t* a, uint64_t* b, uint64_t size)
{
	uint64_t num;
	for(uint64_t i=0; i<size; i++)
		num = a[b[i]];
}

// Modify tests
auto testModifyAddDumb(uint64_t* a, uint64_t* b, uint64_t* c, uint64_t size)
{
	for(uint64_t i=0; i<size; i++)
		c[i] = a[i] + b[i];
}

auto testModifyAddModified(uint64_t* a, uint64_t* b, uint64_t size)
{
	for(uint64_t i=0; i<size; i++)
		a[i] += b[i];
}

auto testModifyIncrease(uint64_t* a, uint64_t size)
{
	for(uint64_t i=0; i<size; i++)
		a[i]++;
}

auto testModifySetConditional(uint64_t* a, uint64_t* b, uint64_t* c, uint64_t size)
{
	for(uint64_t i=0; i<size; i++)
	{
		if(a[i])
			b[i] = c[i];
	}
}

// Prepare
void prepareReset(uint64_t* a, uint64_t size)
{
	memset(a, 0, size);
}

void prepareSparse(uint64_t* a, uint64_t size)
{
	for(uint64_t i=0; i<size; i++)
	{
		a[i] = 0;
		if(i%4<2)
			a[i] = 1;
	}
}

void prepareValue(uint64_t* a, uint64_t size)
{
	memset(a, 1, size);
}

void prepareRow(uint64_t* a, uint64_t size)
{
	for(uint64_t i=0; i<size; i++)
		a[i] = i;
}

void prepareBackwardRow(uint64_t* a, uint64_t size)
{
	for(uint64_t i=0; i<size; i++)
		a[i] = size-i-1;
}

inline time_point<high_resolution_clock> time()
{
	return high_resolution_clock::now();
}

// -------------------------------------------------
// main function
// -------------------------------------------------

int main(int argc, char** argv)
{
	if(argc<3)
	{
		printf("not enough Arguments, Syntax is:\n");
		printf("  %s <sample_size_in_MB> <sample_count>\n", argv[0]);
		return 1;
	}

	uint64_t sample_size_bytes = atoi(argv[1])*1024*1024;
	uint64_t sample_size_objects = sample_size_bytes / sizeof(uint64_t);

	uint32_t sample_count = atoi(argv[2]);

	uint64_t* pointer_a = (uint64_t*)malloc(sample_size_bytes);
	uint64_t* pointer_b = (uint64_t*)malloc(sample_size_bytes);
	uint64_t* pointer_c = (uint64_t*)malloc(sample_size_bytes);

	time_point<high_resolution_clock> start_time, end_time;
	duration<double> diff_time;

	double* durations = new double[11];
	double* bandwidth = new double[11];

	for(uint32_t i=0; i<sample_count; i++)
	{
		// run Write tests
		prepareReset(pointer_a, sample_size_bytes);
		start_time = time();
		testWriteMemset(pointer_a, sample_size_bytes);
		end_time = time();
		diff_time = end_time - start_time;
		durations[0] += diff_time.count();

		prepareReset(pointer_a, sample_size_bytes);
		start_time = time();
		testWriteDumb(pointer_a, sample_size_objects);
		end_time = time();
		diff_time = end_time - start_time;
		durations[1] += diff_time.count();

		// run Copy tests
		prepareReset(pointer_a, sample_size_bytes);
		prepareRow(pointer_b, sample_size_objects);
		start_time = time();
		testCopyMemcpy(pointer_a, pointer_b, sample_size_bytes);
		end_time = time();
		diff_time = end_time - start_time;
		durations[2] += diff_time.count();

		prepareReset(pointer_a, sample_size_bytes);
		start_time = time();
		testCopyDumb(pointer_a, pointer_b, sample_size_objects);
		end_time = time();
		diff_time = end_time - start_time;
		durations[3] += diff_time.count();

		prepareReset(pointer_a, sample_size_bytes);
		prepareRow(pointer_b, sample_size_objects);
		prepareBackwardRow(pointer_c, sample_size_objects);
		start_time = time();
		testCopyByPointer(pointer_a, pointer_b, pointer_c, sample_size_objects);
		end_time = time();
		diff_time = end_time - start_time;
		durations[4] += diff_time.count();

		// run Read tests
		prepareRow(pointer_a, sample_size_objects);
		start_time = time();
		testReadDumb(pointer_a, sample_size_objects);
		end_time = time();
		diff_time = end_time - start_time;
		durations[5] += diff_time.count();

		prepareRow(pointer_a, sample_size_objects);
		prepareBackwardRow(pointer_b, sample_size_objects);
		start_time = time();
		testReadByPointer(pointer_a, pointer_b, sample_size_objects);
		end_time = time();
		diff_time = end_time - start_time;
		durations[6] += diff_time.count();

		// run Modify tests
		prepareReset(pointer_a, sample_size_bytes);
		prepareRow(pointer_b, sample_size_objects);
		prepareRow(pointer_c, sample_size_objects);
		start_time = time();
		testModifyAddDumb(pointer_a, pointer_b, pointer_c, sample_size_objects);
		end_time = time();
		diff_time = end_time - start_time;
		durations[7] += diff_time.count();

		prepareValue(pointer_a, sample_size_bytes);
		prepareRow(pointer_b, sample_size_objects);
		start_time = time();
		testModifyAddModified(pointer_a, pointer_b, sample_size_objects);
		end_time = time();
		diff_time = end_time - start_time;
		durations[8] += diff_time.count();

		prepareValue(pointer_a, sample_size_bytes);
		start_time = time();
		testModifyIncrease(pointer_a, sample_size_objects);
		end_time = time();
		diff_time = end_time - start_time;
		durations[9] += diff_time.count();

		prepareSparse(pointer_a, sample_size_objects);
		prepareRow(pointer_b, sample_size_objects);
		prepareBackwardRow(pointer_c, sample_size_objects);
		start_time = time();
		testModifySetConditional(pointer_a, pointer_b, pointer_c, sample_size_objects);
		end_time = time();
		diff_time = end_time - start_time;
		durations[10] += diff_time.count();
	}
	for(uint8_t i=0; i<11; i++)
	{
		printf("test %u took %lf s on average, getting to %lf MB/s bandwidth\n", i, durations[i]/sample_count, sample_size_bytes/1024/1024/(durations[i]/sample_count));
	}

	return 0;
}
